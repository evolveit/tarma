module Tarma
  class QueryStore
    include Enumerable
    attr_accessor :queries

    def initialize(queries = [])
      self.queries = queries
    end

    def each &block
      @queries.each do |queries|
        if block_given?
          block.call queries
        else
          yield queries
        end
      end
    end

    def << query
        self.queries << query
    end

    def [] index
      self.queries[index]
    end

    def size
      self.queries.size
    end

    ##
    # Execute each query
    def execute
      progressbar = ProgressBar.create(title: 'Executing and evaluating queries',total: size, output: STDERR, format: "%a %e %P% Processed: %c from %C")
      # print header
      CSV {|row| row << self.queries.first.csv_header}
      self.queries.delete_if do |query|
        query.call
        query.evaluate
        # print result
        CSV {|row| row << query.to_csv_row}
        # print recommendation list to file
        query.result.print_to_file("#{query.output}"+query.id.to_s+"_"+query.algorithm)
        progressbar.increment
        # return true so the query is deleted after completion
        true
      end
    end

    def to_csv
      p = ProgressBar.create(title: "Generating CSV", total: size, output: STDERR)
      if self.size > 0
          CSV {|row| row << self.queries.first.csv_header}
          self.each do |q|
            CSV {|row| row << q.to_csv_row}
            p.increment
          end
      else
        ""
      end
    end
  end
end

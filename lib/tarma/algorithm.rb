#file algorithm.rb
module Tarma
  class Algorithm

    def initialize(query)
      if query.is_a?(Tarma::Query)
        @tx_store = Tarma::HistoryStore.get_history(query.cutoff_start,query.cutoff_end,query.max_size)
        @query = query.query
        @algorithm = query.algorithm
        # if a svd based algo is used, calculate the svd
        if !(@algorithm =~ /svd/).nil? 
          @svd = Tarma::HistoryStore.get_svd(query.cutoff_start,query.cutoff_end,query.max_size) 
        end
      end
    end

    def execute
      if !(@algorithm =~ /svd/).nil?
        if threshold = /(?<num>0\.\d+)/.match(@algorithm)
          svd_algorithm(threshold[:num].to_f)
        else
          svd_algorithm
        end
      else
        self.method(algorithm+'_algorithm').call
      end
    end

    ###
    # tarmaq
    ###
    def tarmaq_algorithm
      k = 0
      filtered_history = Tarma::TxStore.new
      # filtering
      @tx_store.each do |tx|
        # get interaction of transaction and query
        intersection = tx.items & @query
        # check if intersection size is equal to current largest intersection size
        if intersection.size == k
          filtered_history << tx
        elsif intersection.size > k
          # there is a new largest querysubset
          k = intersection.size
          # reset the history
          filtered_history.clear
          filtered_history << tx
        end
      end
      # rule creation
      result = Tarma::Result.new
      # to be able to update rules we must keep track of the antecedent
      antecedents = Hash.new
      filtered_history.each do |tx|
        # the intersection is the antecedent of the rule
        # we sort since we use the antecedent as a key in
        # the antecedents hash
        antecedent = (tx.items & @query).sort
        consequents = tx.items - antecedent
        # update antecedent count
        if antecedents[antecedent].nil?
          antecedents[antecedent] = 0
        end
        antecedents[antecedent] += 1
        consequents.each do |consequent|
          rule_name = "#{antecedent.join(",")} -> #{consequent}"
          # support increases with 1/history_size
          if rule = result.get_rule_by_name(rule_name)
            new_support = rule.support + 1.to_r/filtered_history.size
            rule.support = new_support
          else
            # new rule
            support = 1.to_r/filtered_history.size
            result << Tarma::Rule.new(antecedent,consequent,support:support)
          end
        end
        # update confidence of all rules with the current antecedent
        # confidence is support/support_of_antecedent
        result.get_rules_with_antecedent(antecedent).each do |rule|
          new_confidence = rule.support/(antecedents[antecedent].to_r/filtered_history.size)
          rule.confidence = new_confidence
        end

      end
      # (sorting of rules is done through the Comparable mixin in the Rule class)
      result
    end

    ##
    # rose
    ##
    def rose_algorithm
      result = Tarma::Result.new
      # transcations where @query is a subset
      # array of arrays/transactions
      relevant_txes = @tx_store.transactions_of_list(@query,true).map(&:items)
      result.relevant_txes_count = relevant_txes.size
      if !relevant_txes.empty?
        # for each item changed with at least one item from the query
        relevant_txes.array_difference(@query).each do |item|
          frequency = relevant_txes.select {|t| t.include?(item)}.size
          confidence = frequency.to_r/relevant_txes.size
          support = confidence
          result << Tarma::Rule.new(@query,[item],support:support,confidence:confidence)
        end
      end
      result
    end



    ##
    # svd
    ##
    def svd_algorithm(threshold = 0)
      result = Tarma::Result.new
      perfect_match,clusters = @svd.clusters(@query,threshold)
      # check for any perfect matches in clusters
      if !perfect_match.empty?
        perfect_match.each do |(index,sign)|
          s_value = @svd.s[index]
          clusters[index][sign][:clustered].each do |(clustered_item,cluster_weight)|
            result << Tarma::Rule.new(@query,[clustered_item],support:s_value,confidence:cluster_weight.abs)
          end
        end
      else
        # create rules for all items that are clustered with something in the query
        clusters.each do |index,cluster|
          s_value = @svd.s[index]
          [:pos,:neg].each do |sign|
            query_match = cluster[sign][:query_match]
            if query_match.size > 0
              cluster[sign][:clustered].each do |(clustered_item,cluster_weight)|
                result << Tarma::Rule.new(query_match,[clustered_item],support:s_value,confidence:cluster_weight.abs)
              end
            end
          end
        end
      end
      result
    end
  end # Algorithm
end

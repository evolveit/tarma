module Tarma
  class TxStore
    include Enumerable
    attr_reader :txes, :items

    def initialize(txes = [])
      if txes.empty?
        @txes = txes
      else
        @txes = []
        txes.each do |tx|
          self.txes << tx
        end
      end
      @items = Hash.new
    end

    ##
    # self << tx
    #
    # adds <tx> and updates @items with which items are changed in which tx
    def << tx
      if tx.respond_to?(:items)
    		tx.items.each do |item|
    			if !@items.key?(item)
    				@items[item] = [tx]
    			else
    				@items[item] << tx
    			end
    		end
        @txes << tx
      else
        raise Tarma::Exceptions::NotATransaction.new(tx)
      end
    end

    # implementing #each gives us access to all Enumerable methods
    # select, find_all etc
    def each &block
      @txes.each do |tx|
        if block_given?
          block.call tx
        else
          yield tx
        end
      end
    end

    def first
      @txes.first
    end

    def last
      @txes.last
    end

    def [] index
      @txes[index]
    end

    def to_s
      history = ""
      self.txes.each do |tx|
        history << tx.items.to_s+"\n"
      end
      history
    end
    
    ##
    # clear out the currently loaded transactions
    def clear
      @txes.clear
      @items.clear
    end

    ##
    # get_tx(tx_id)
    #
    # returns the tx with id == tx_id
    def get_tx_by_id(tx_id)
      @txes.find {|tx| tx.id == tx_id }
    end

    ##
    # get_tx(tx_index)
    #
    # returns the tx with index == tx_index
    def get_tx_by_index(tx_index)
      @txes[tx_index]
    end

    def size
      @txes.size
    end


  	##
  	# read in a JSON file of transactions
    #
    # TRANSACTIONS ARE LOADED IN REVERSE ORDER!
    #
    # this implies that the oldest transaction gets index 0 in the txes array
    # and the newest has index txes.size-1
    # (givent that the json file is sorted from newest/top to oldest/bottom)
    def load_transactions(path: nil, before: nil, after: nil)
      if !path.nil?
        JSON.parse(File.read(path,
                             external_encoding: 'iso-8859-1',
                             internal_encoding: 'utf-8')).reverse.each do |json_object|
          begin
            if valid_date?(json_object,before,after)
              self << json_to_tx(json_object)
            end
          rescue Tarma::Exceptions::NoChangedItemsInChanges => e
            STDERR.puts e
            next # skip to next tx/object
          rescue Tarma::Exceptions::NoChangesInJsonObject => e
            STDERR.puts e
            next
          rescue Tarma::Exceptions::UnableToConvertJsonToTx => e
            STDERR.puts e
            next
          rescue Tarma::Exceptions::NoDateInJsonObject => e
            STDERR.puts e
            next
          rescue Tarma::Exceptions::OnlyNilInChanges => e
            STDERR.puts e
            next
          end
        end
      end
    end

    def json_to_tx(json)
        tx = nil
        id = json["sha"]
        date = json["date"]
        if !json["changes"].nil?
          if items = json["changes"]["all"]
            if !items.compact.empty?
              tx = Tarma::Tx.new(self.size,id,date,items.compact)
            else
              raise Tarma::Exceptions::OnlyNilInChanges.new, "#{json["sha"]} \"changes\"\"all\" field only contained nil value(s)"
            end
          else
            raise Tarma::Exceptions::NoChangedItemsInChanges.new, "#{json["sha"]} did not have any \"changes\"\"all\" field"
          end
        else
          raise Tarma::Exceptions::NoChangesInJsonObject.new, "#{json["sha"]} did not have a \"changes\" field"
        end
        if tx.nil?
          raise Tarma::Exceptions::UnableToConvertJsonToTx.new, "#{json["sha"]} could not be converted to a tx"
        else
          tx
        end
    end

    ##
    # a looser version of #between?
    # we also allow nil comparisons
    # if both <after> and <before> are nil we consider the date valid
    def valid_date?(json_object,after,before)
      if date = json_object["date"]
        if after.nil? & before.nil?
          return true
        elsif !after.nil? & !before.nil?
          if date.between?(after, before)
            return true
          end
        elsif !after.nil?
          if date > after
            return true
          end
        elsif !before.nil?
          if date < before
            return true
          end
        end
      else
        raise Tarma::Exceptions::NoDateInJsonObject.new, "#{json_object["sha"]} had no \"date\" field."
      end
      return false
    end

    ##
    # #get_cloned_subset
    #
    # Returns a clone of <self> with transactions equal to the index range defined
    # by
    #  from and including <start_index> to and including <stop_index>
    # also exclude transactions with size larger than <max_size>
    def clone_with_subset(start_index,stop_index,max_size = nil)
      clone = TxStore.new
      if start_index.nil? & stop_index.nil? & max_size.nil? then return self end
        # if only one of start_index and stop_index is provided, raise exception
      if !start_index.nil? ^ !stop_index.nil?
         raise ArgumentError.new "You must provide both a start and end index"
      end
      # check that its a valid range
      if range = self.txes[start_index..stop_index]
        if max_size.nil?
          range.each do |tx|
            clone << tx
          end
        else
          range.select {|tx| tx.size <= max_size}.each do |tx|
            clone << tx
          end
        end
      else
        raise ArgumentError.new, "#{start_index}..#{stop_index} was not a valid range on tx_store with size #{self.size}"
      end
      clone
    end

  	############################################
  	# HELPERS                                  #
  	#                                          #
  	# These are meant to be used by all        #
  	# algorithm implementations if needed.     #
  	# Having these methods here eases testing. #
  	############################################

  	# Given a testable, find those transactions
  	# where the testable has been modified
  	# parameters:
  	# item: the item to check
  	def transactions_of(item)
  		self.items.key?(item) ? self.items[item] : []
  	end


  	# As transactions, but returns just the ids
  	def transaction_ids_of(item)
      transactions_of(item).map(&:id)
  	end

    def transaction_indexes_of(item)
      transactions_of(item).map(&:index)
    end

  	##
  	# Returns the relevant transactions of the query
  	# That is: all the transactions where at least one
  	# item from the query were changed
  	#
  	# parameters:
  	# query: a list of items
  	# (optional) strict: if set to true, all the items of the query has had
  	# to be changed in the transaction for it to be included
  	def transactions_of_list(items, strict = false)
  		transaction_ids_of_list(items, strict).map { |id| self.get_tx_by_id(id)  }
  	end

  	##
  	# Returns the relevant transaction ids of the query
  	# That is: all the transactions where at least one
  	# item from the query were changed
  	#
  	# parameters:
  	# query: a list of items
  	# (optional) strict: if set to true, all the items of the query has had
  	# to be changed in the transaction for it to be included
  	def transaction_ids_of_list(items, strict = false)
  		if strict
  			items.map {|item| transaction_ids_of(item)}.array_intersection
  		else
  			items.map {|item| transaction_ids_of(item)}.array_union
  		end
  	end

  	##
  	# Return the list of items that have changed
  	# with at least one item from the query
  	def relevant_unchanged_items(query)
  		transactions_of_list(query).map(&:to_a).array_union - query
  	end

    ##
    # returns true if all items have changed in the history
    def all_previously_changed?(items) 
      items.each do |item|
        if transactions_of(item).empty?
          return false
        end
      end
      true
    end

  	##
  	# Returns true if the items in this transaction
  	# never has been changed together before
  	#
  	# params:
  	#     id:          the id of the transaction
  	def not_a_previous_subset?(id)
  		raise "must be reimplemented (talk to thomas)"
  		tx_query = self.txes[id]
  		self.txes.select {|k,v| k < id}.each do |k,v|
  			if tx_query.subset?(v)
  				return false
  			end
  		end
  		true
  	end


  	##
  	# Returns the number of times the items of the given transaction
  	# has previously been changed all together.
  	#
  	# params:
  	#     id:          the id of the transaction
  	def previous_supersets_count(id)
  		raise "must be reimplemented (talk to thomas)"
  		tx_query = self.txes[id]
  		self.txes.select {|k,v| k < id && tx_query.subset?(v)}.size
  	end

  	##
  	# Returns the number of previous transactions
  	# where any of the items of the query has been
  	# changed
  	#
  	# parameters:
  	# id => the commits position in the self.txes hash
  	def previous_membersets_count(id)
  		raise "must be reimplemented (talk to thomas)"
  		tx_query = self.txes[id]
  		size = 0
  		if txes = self.txes.select {|k,v| k < id && tx_query.include_any?(v)}
  			size = txes.size
  		end
  		size
  	end

    ##
    # return a (string) json representation of the tx_store
    def to_json 
      commits = Hash.new {|h,k| h[k] = Hash.new(&h.default_proc) }
      self.each do |tx|
            sha = tx.id
            commits[sha][:sha]            = sha
            commits[sha][:date]           = tx.date
            commits[sha][:index]          = tx.index
            commits[sha][:changes][:all]  = []
            tx.items.each {|item| commits[sha][:changes][:all] << item}
      end
      # print the commits sorted by index 
      # but dont include the index in the json as there might be "holes" (after filtering etc)
      JSON.pretty_generate(commits.sort_by {|id,commit| commit[:index]}.reverse.map {|(sha,commit)| commit.tap {|c| c.delete(:index)}})
    end

    def to_transactions
      self.txes.reverse.each {|tx| CSV {|row| row << tx.items}}
    end

  private
  	def parse_date date
  		if !date.nil?
  			begin
  				Time.parse date
  			rescue TypeError => e
  				# something else than string was given as input
  				$stderr.puts "Unable to parse #{date}, error: " + e
  			rescue ArgumentError => e
  				# unable to parse the string for a date
  				$stderr.puts "Unable to parse #{date} for a date, error: " + e
  			end
  		end
  	end
  end
end

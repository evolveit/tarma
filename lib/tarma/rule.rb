module Tarma
  class Rule
    include Comparable
    attr_accessor :lhs, :rhs, :support, :confidence, :query_coverage, :weight

    def initialize(lhs,rhs,weights = {support: 0,confidence: 0,query_coverage: 0,weight: 0})
      self.lhs = lhs
      self.rhs = rhs
      self.support        = weights[:support].nil?        ? 0 : weights[:support]
      self.confidence     = weights[:confidence].nil?     ? 0 : weights[:confidence]
      self.query_coverage = weights[:query_coverage].nil? ? 0 : weights[:query_coverage]
      self.weight         = weights[:weight].nil?         ? 0 : weights[:weight]
    end

    def <=> other
      return nil unless other.is_a?(Tarma::Rule)
      (other.support <=> self.support).nonzero? ||
      (other.query_coverage <=> self.query_coverage).nonzero? ||
      (other.confidence <=> self.confidence).nonzero? ||
      (other.weight <=> self.weight).nonzero? ||
      0
    end

    def name
			"#{self.lhs.join(",")} -> #{self.rhs.join(",")}"
    end

    def to_s
      support.to_s + " " + confidence.to_s + " " + query_coverage.to_s + " " + weight.to_s + " " + name
    end

    def to_csv
      [support,confidence,query_coverage,weight,name]
    end

    def lhs=input
      input.is_a?(Array) ? @lhs = input : @lhs = [input]
    end

    def rhs=input
      input.is_a?(Array) ? @rhs = input : @rhs = [input]
    end
  end
end

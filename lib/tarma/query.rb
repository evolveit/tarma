module Tarma
  class Query
    include Comparable
    ## if using jruby, include the Callable mixin for concurrency
    if RUBY_PLATFORM == 'java' then include Callable end

    attr_accessor
                  :algorithm,
                  :query,
                  :time,
                  :opts,
                  :transactions,
                  :output,
                  :result,
                  :tx_store

    def initialize(opts)
      # fields for csv
      self.algorithm=        opts[:algorithm].nil? ? opts['algorithm'] : opts[:algorithm]
      self.query=            opts[:query].nil? ? opts['query'] : opts[:query]
      self.time=             opts[:time].nil? ? opts['time'] : opts[:time]

      # internal fields
      self.opts=             opts
      self.transactions=     opts[:transactions]
      self.output=           opts[:output]
      self.result=           Tarma::Result.new

      # class variables
      # we assume that all current Query instances are operating on the same
      # history/transactions
      if Tarma::HistoryStore.base_history.nil? & !opts[:transactions].nil?
        base_txstore = Tarma::TxStore.new
        base_txstore.load_transactions(path: opts[:transactions])
        Tarma::HistoryStore.base_history = base_txstore
      end
    end


    ##
    # <=> defines how to compare two Query objects
    def <=> other
      return nil unless other.is_a?(Query)
      comparison = 0
      # first we compare the tx id
      if (self.tx_id <=> other.tx_id) == 0
        # if we also have the same query
        if (self.query.sort <=> other.query.sort) == 0
        # use history size as comparator
           comparison = (self.cutoff_size <=> other.cutoff_size)
        else
          # use the query
          comparison = (self.query.sort <=> other.query.sort)
        end
      else
        # use the tx id
        comparison = (self.tx_id <=> other.tx_id)
      end
      comparison
    end

    def call
      a = Algorithm.new(self)
      t1 = Time.new
      self.result = a.execute
      t2 = Time.new
      self.time = TimeDifference.between(t1,t2).in_seconds.round(4)
      if self.result.empty?
        STDERR.puts "Could not recommend anything based on the #{self.query.size} item(s) in your query"
      end
    end





    ##
  	# custom setter for query
    def query=(query)
      if query.nil? || query.empty?
        raise Tarma::Exceptions::QueryNilOrEmpty.new, "#{self.id} did not have a query"
      end
      if query.is_a?(Array)
        if query.size == 1
          query = query.first
        else
          @query = query
        end
      end
      if query.is_a?(String)
        #check if input is using process substition
        if !(/\/dev\/fd\// =~ query).nil?
          @query = File.read(query).split("\n")
        else
          @query = query.split(",")
        end
      end
      # when we set a new query, allways clear out any previously
      # generated rules
      if !self.result.nil? then self.result.clear end
    end

    ##
    #
    def instance_values_for_csv
      dont_include = ['opts', 'tx_store', 'transactions', 'result','output']
      self.instance_values.delete_if {|k,v| dont_include.include?(k)}
    end

    ##
    # generate an array suitable for a csv header
    def csv_header
      query = self.instance_values_for_csv.keys
      result = self.result.nil? ? [] : self.result.csv_header
      result + query
    end

    ##
    # generate an array of the current values of <self>
    # converts any array values to a comma separated string representation
    def to_csv_row
      query = self.instance_values_for_csv.values.map {|val| val.is_a?(Array) ? val.join(',') : val}
      result = self.result.nil? ? [] : self.result.to_csv_row
      result + query
    end



  	##
  	# Prints the rules to standard out
  	# sorted by strength
  	def print
      if self.result.nil?
        puts "This query has not been executed yet"
      else
        STDERR.puts "Recommending using #{self.algorithm}\n"
        self.result.print
      end
  	end

  end
end

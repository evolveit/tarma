module Tarma
  class HistoryStore

    # create accessors for class level instance variables
    class << self
      attr_accessor :base_history, :tag, :history, :svd
    end

    def self.get_history(start_index,end_index,max_size=nil)
      tag = gen_tag(start_index,end_index,max_size)
      if self.tag != tag
        # new history
        self.tag = tag
        # create new subset
        self.history = self.base_history.clone_with_subset(start_index,end_index,max_size)
      end
      self.history
    end

    def self.get_svd(start_index,end_index,max_size=nil)
      tag = self.gen_tag(start_index,end_index,max_size)
      if self.svd.nil? || (self.tag != tag)
        self.svd = Tarma::SVD.new(get_history(start_index,end_index,max_size)) 
      end
      self.svd
    end

    private
    def self.gen_tag(start_index,end_index,max_size)
      start_index.to_s+end_index.to_s+max_size.to_s
    end
  end # HistoryStore
end # Tarma

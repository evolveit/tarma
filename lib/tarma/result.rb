module Tarma
  class Result
    include Enumerable
    attr_accessor :rules, :relevant_txes_count, :average_precision, :top10_recall, :top10_precision

    def initialize(rules = [])
      self.rules = rules
      self.relevant_txes_count         = 0
      self.average_precision           = 0
      self.top10_recall                = 0
      self.top10_precision             = 0
    end

    def each &block
      @rules.each do |rule|
        if block_given?
          block.call rule
        else
          yield rule
        end
      end
    end

    def get_rule_by_name(name)
      @rules.find {|rule| rule.name == name}
    end

    def get_rules_with_antecedent(antecedent)
      @rules.find_all {|rule| rule.lhs.sort == antecedent.sort}
    end

    ##
    # returns an ordered recommendation based on the current rules
    # items (rhs) are ordered by the weight of its rule
    def recommendation
      self.sort.map(&:rhs).flatten
    end

    def << rule
      self.rules << rule
    end

    def to_s
      recommendation = %w(support confidence query_coverage weight rule)
      self.sort.each do |rule|
        recommendation << rule.to_csv.to_s
      end
      recommendation
    end

    def print
      CSV {|row| row << %w(support confidence query_coverage weight rule)}
      self.sort.each do |rule|
        CSV {|row| row << rule.to_csv}
      end
      return nil
    end

    def print_to_file(filename)
      CSV.open(filename, "wb") do |csv| 
        csv << %w(support confidence query_coverage weight rule)
        self.sort.each do |rule|
          csv << rule.to_csv
        end
      end
    end

    def size
      self.rules.size
    end

    def empty?
      self.rules.empty?
    end

    def clear
      self.rules.clear
    end

    def evaluate(expected_outcome)
      self.calc_top10(expected_outcome)
      self.calc_average_precision(expected_outcome)
    end

    def calc_top10(expected_outcome)
        if (expected_outcome.size > 0) & !self.empty?
          # get top10 recommendations
          top10 = self.recommendation.take(10)
          common_items = (expected_outcome & top10).size.to_r
          self.top10_precision = (common_items/top10.size).to_f
          self.top10_recall = (common_items/expected_outcome.size).to_f
        else
          self.top10_recall = nil
          self.top10_precision = nil
        end
    end

    # calculate the average precision of the result based on an expected outcome
    def calc_average_precision(expected_outcome)
        if !expected_outcome.is_a?(Array) then expected_outcome = [expected_outcome] end
        if (expected_outcome.size > 0) & !self.empty?
          average_precision = 0
          correct_items = []
          total_items_considered = []
          # sort rules by weight
          # we first group rules with equal weights
          # and then sort the groups by weight
          self.sort.each do |rule|
            items = rule.rhs
            # skip already considered items
            if (new_items = items - total_items_considered).size > 0
              new_items.each {|item| total_items_considered << item}
              if correct_in_rule = (items & expected_outcome)
                if correct_in_rule.size > 0
                  # make sure that the new items havent already been added earlier
                  new_correct = (correct_in_rule - correct_items)
                  # add new items
                  new_correct.each {|item| correct_items << item}
                  change_in_recall = new_correct.size.to_r/expected_outcome.size
                  precision_at_k = correct_items.size.to_r/total_items_considered.size
                  average_precision += (precision_at_k * change_in_recall)
                end
              end
            end
          end
          self.average_precision = average_precision.to_f
        else
          self.average_precision = nil
        end
    end


    def instance_values_for_csv
      dont_include = ['rules']
      self.instance_values.delete_if {|k,v| dont_include.include?(k)}
    end

    ##
    # generate an array suitable for a csv header
    def csv_header
      self.instance_values_for_csv.keys
    end

    ##
    # generate an array of the current values of <self>
    # converts any array values to a comma separated string representation
    def to_csv_row
      self.instance_values_for_csv.values.map {|val| val.is_a?(Array) ? val.join(',') : val}
    end
  end
end

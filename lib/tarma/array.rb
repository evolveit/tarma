class Array
	def subset?(other)
		self & other == self
	end

	def include_any?(other)
		(self & other).size > 0
	end

	##
	# returns the union of an array of arraya
	def array_union
		if union = self.inject(:|)
			return union
		else
			return []
		end
	end

	##
	# returns the intersection of a list of lists
	def array_intersection
		if intersection = self.inject(:&)
			return intersection
		else
			return []
		end
	end

	##
	# returns the list of items in self that was not in other
	def array_difference(other)
		self.map {|a| a - other}.array_union
	end

end

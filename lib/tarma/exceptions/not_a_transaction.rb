module Tarma
  module Exceptions
    class NotATransaction < StandardError
    end
  end
end

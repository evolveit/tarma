module Tarma
  module Exceptions
    class NoChangesInJsonObject < StandardError
    end
  end
end

module Tarma
  module Exceptions
    class UnableToConvertJsonToTx < StandardError
    end
  end
end

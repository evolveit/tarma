module Tarma
  module Exceptions
    class NoResult < StandardError
    end
  end
end

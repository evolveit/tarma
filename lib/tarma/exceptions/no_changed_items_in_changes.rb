module Tarma
  module Exceptions
    class NoChangedItemsInChanges < StandardError
    end
  end
end

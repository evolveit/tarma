module Tarma
  module Exceptions
    class NotAResult < StandardError
    end
  end
end

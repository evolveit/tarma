module Tarma
  module Exceptions
    class NoDateInJsonObject < StandardError
    end
  end
end

module Tarma
  module Exceptions
    class QueryNilOrEmpty < StandardError
    end
  end
end
